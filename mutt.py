#!/usr/bin/env python

import logging
import os
import pynotify
import subprocess
import sys

import indicate
import gtk

#from gi.repository import Indicate as indicate
#from gi.repository import Gtk as gtk


# How frequently to check for new messages, in seconds
CHECK_FREQUENCY = 300

# Hash of maildirs in "FriendlyName : PathWithTrailingSlash" format
MAILDIRS = {
    "smita": "/home/smita/Mail/smita.info/",
    "gmail": "/home/smita/Mail/gmail.com/",
}

# Tuple of getmail config files
GETMAIL_CONFIGS = (
    'config/smita_info',
    'config/gmail'
)

# Indicator entry requires a desktop. It uses it for name & icon.
# As this will be passed to an external process, the file must be an absolute path.
MUA_DESKTOP_FILE = os.path.abspath(os.path.dirname(sys.argv[0])) + "/mutt.desktop"

# Workaround for Ubuntu bug#378783: "xdg-open *.desktop opens text editor"
# https://bugs.launchpad.net/ubuntu/+source/xdg-utils/+bug/378783
# Due to this bug, trying to xdg-open the MUA_DESKTOP_FILE will instead open it
# in a text editor. We *could* parse the EXEC line, but would miss TERMINAL=true,
# and possibly other things.
MUA_LAUNCH_COMMAND = "/usr/bin/gnome-terminal -e mutt &"

# Getmail command
GETMAIL_COMMAND = '/usr/bin/getmail'

# Title for notification bubbles
NOTIFY_TITLE = "Mutt Indicator"


class MuttIndicatorError(Exception):
    pass


class Log(object):
    LOGFILE = os.path.join(os.path.normpath(os.path.dirname(__file__)), 'log', 'indicator_mutt.log')
    LEVEL = logging.DEBUG
    FORMAT_STRING = '%(asctime)s %(levelname)s: %(message)s'

    def __init__(self):
        logging.basicConfig(filename=self.LOGFILE, level=self.LEVEL, format=self.FORMAT_STRING)
        self.log = logging.getLogger(__name__)

    def __getattr__(self, name):
        return getattr(self.log, name)

log = Log()


class MuttIndicator(object):
    """
    Indicate incoming mails.
    """

    def __init__(self):
        self.retrieving_mails = False
        self.notified_messages = self._init_notified_messages()

        self.indicator = indicate.indicate_server_ref_default()
        self.indicator.set_type("message.mail")
        self.indicator.set_desktop_file(MUA_DESKTOP_FILE)
        self.indicator.connect("server-display", self.indicator_clicked)
        self.indicators = {}

        # Register with notification system
        if pynotify.init("Mutt Indicator"):
            self.notify = True
        else:
            self.notify = False

        # Build list of maildir indicators
        self.build_menus()

    def _init_notified_messages(self):
        notified_messages = {}
        for name, path in MAILDIRS.iteritems():
            notified_messages[name] = set()

        return notified_messages

    def _get_count_indicate_messages(self, name):
        try:
            count = int(self.indicators[name].get_property("count"))
        except ValueError:
            count = 0
        return count

    def _get_new_messages(self, name):
        try:
            msgs = set(os.listdir(os.path.join(path, 'new')))
        except OSError:
            log.error('Can\'t read new messages.')
            raise MuttIndicatorError

        return msgs

    def build_menus(self):
        for name, path in MAILDIRS.iteritems():
            # Create mailbox indicator
            new_indicator = indicate.Indicator()
            new_indicator.set_property("name", name)
            new_indicator.set_property("count", "0")
            new_indicator.label = name
            new_indicator.connect("user-display", self.maildir_clicked)

            # Always show it
            new_indicator.show()

            # Save it for later
            self.indicators[name] = new_indicator

    def maildir_clicked(self, widget, data=None):
        # Not sure how to do this nicely.
        # no-op it for now.
        os.system(MUA_LAUNCH_COMMAND)
        return True

    def indicator_clicked(self, widget, data=None):
        # Run MUA_LAUNCH_COMMAND
        os.system(MUA_LAUNCH_COMMAND)
        return True

    def send_notify(self, message):
        if not self.notify:
            log.debug("Bypassing notification")
            return True

        log.debug("Sending notification")
        n = pynotify.Notification(NOTIFY_TITLE, message, "notification-message-email")
        n.show()

        return n

    def get_mails(self):
        if not self.retrieving_mails:
            log.info("Begin of retrieving_mails...")
            self.retrieving_mails = True
            cmd = [GETMAIL_COMMAND, '-v']
            cmd.extend(map(lambda x: "-r%s" % x, GETMAIL_CONFIGS))
            try:
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                log.error('Getmail faild!!!')
            self.retrieving_mails = False
            log.info(output)
            log.info("End of retrieving_mails...")
        else:
            log.info("Skipping retrieving_mails")

        return True

    def check_mails(self):
        for name, path in MAILDIRS.iteritems():
            try:
                msgs = set(os.listdir(os.path.join(path, 'new')))
                diff = msgs.difference(self.notified_messages[name])
                new_messages_count = len(diff)

                self.indicators[name].set_property('count', str(len(msgs)))

                if new_messages_count > 0:
                    self.indicators[name].set_property('draw-attention', 'true')
                    self.send_notify('You have %d new masages in %s mailbox.' % (new_messages_count, name))
                else:
                    self.indicators[name].set_property('draw-attention', 'false')

                self.notified_messages[name] = msgs

            except MuttIndicatorError:
                self.indicators[name].set_property('count', 'error')
                self.indicators[name].set_property('draw-attention', 'false')

        return True

    def fetch_and_check(self):
        self.get_mails()
        self.check_mails()
        return True

    def run(self):
        # This is the main loop
        self.fetch_and_check()
        gtk.timeout_add(CHECK_FREQUENCY * 1000, self.fetch_and_check)
        gtk.main()


if __name__ == "__main__":
    indicator = MuttIndicator()
    indicator.run()
